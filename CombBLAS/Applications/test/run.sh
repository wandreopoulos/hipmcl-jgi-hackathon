
git clone  git clone https://wandreopoulos@bitbucket.org/berkeleylab/combinatorial-blas-2.0.git
git checkout 035a8a8
Ariful Azad committed 035a8a8
2018-11-16
https://bitbucket.org/berkeleylab/combinatorial-blas-2.0/commits/035a8a8fc1e339cd27ac49329d2b53519df7a3c5






[andreopo@login1.ascent Applications]$ git diff makefile-summit
[andreopo@login1.ascent Applications]$ make -f makefile-summit  mcl


nvprof

jsrun -n1 -c21 -bpacked:21 -a1


bsub  -P GEN123  -nnodes  1  -W  30  -Is /bin/bash


export OMP_NUM_THREADS=1
module load cuda
module load gcc/5.4.0

jsrun -n1 -c21 -bpacked:21 -a1  ../mcl   -M  ../../../../data/vir_vs_vir_30_50length.indexed.triples  -I 2 -per-process-mem 96 -o  ../../../../data/vir_vs_vir_30_50length.indexed.triples.hipmclcpus

jsrun -n1 -c21 -bpacked:21 -a1  nvprof  ../mcl   -M  ../../../../data/vir_vs_vir_30_50length.indexed.triples  -I 2 -per-process-mem 16 -o  ../../../../data/vir_vs_vir_30_50length.indexed.triples.hipmclcpus_nvprof

jsrun -n1 -c21 -bpacked:21 -a1  nvprof  -f -o vir_vs_vir_30_50length.indexed.triples.hipmclcpus_nv.prof  --cpu-profiling on  ../mcl   -M  ../../../../data/vir_vs_vir_30_50length.indexed.triples  -I 2 -per-process-mem 16 -o  ../../../../data/vir_vs_vir_30_50length.indexed.triples.hipmclcpus_nvprof



bash-4.2$ jsrun -n1 -c21 -bpacked:21 -a1  nvprof  -f -o vir_vs_vir_30_50length.indexed.triples.hipmclcpus_nv.prof  --cpu-profiling on  ../mcl   -M  ../../../../data/vir_vs_vir_30_50length.indexed.triples  -I 2 -per-process-mem 16 -o  ../../../../data/vir_vs_vir_30_50length.indexed.triples.hipmclcpus_nvprof



bash-4.2$ jsrun -n1 -c21 -bpacked:21 -a1  nvprof  -s  -f -o virus_nv.prof.NVTX  --cpu-profiling on  ../mcl   -M  ../../../../data/vir_vs_vir_30_50length.indexed.triples  -I 2 -per-process-mem 16 -o  ../../../../data/virus_nv.prof.NVTX


This gives us a profile at least:

  173  jsrun -n1 -c21 -bpacked:21 -a1  ../mcl   -M  ../../../../data/vir_vs_vir_30_50length.indexed.triples.SMALL   -I 2 -per-process-mem 96 -o  ../../../../data/vir_vs_vir_30_50length.indexed.triples.SMALL.gprof
  174  l
  175  ls -latr
  176  mv gmon.out gmon.virusSMALL.out
  177  ls -latr
  178  cd ..
  179  pwd
  180  ls -latr
  181  ls -latr test/
  182  gprof mcl
  183  gprof -h
  184  gprof mcl  test/gmon.virusSMALL.out 




A very fast run:
bash-4.2$  jsrun -n1 -c21 -bpacked:21 -a1  /ccsopen/proj/gen123/JGP/hipmcl/CombBLAS/Applications/mcl   -M  ../../../../data/sevenvertex.triples  -I 2 -per-process-mem 16 -o  ../../../../data/sevenvertex.triples.JAsmyn.out




Ignore this it is too slow atm:
New command that removed SpGemm and uses SpHybridGemm for speed and --matrix-market  used  .mtx flags:

bash-4.2$ jsrun -n1 -c21 -bpacked:21 -a1    /ccsopen/proj/gen123/JGP/hipmcl.new/combinatorial-blas-2.0/CombBLAS/Applications/mcl    -M  ../../../../data/Renamed_vir_vs_vir_30_50length.indexed.mtx   -I  2  -per-process-mem 96  --32bit-local-index  --matrix-market -base 0  -o  ../../../../data/Renamed_vir_vs_vir_30_50length.indexed.mtx.hipmclcpus_nvproftest                     

