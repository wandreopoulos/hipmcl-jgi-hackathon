#!/bin/bash -l

#SBATCH -q regular
#SBATCH -N 16
#SBATCH -C knl
#SBATCH -t 11:00:00
#SBATCH -J WOOD_DECAYERS_cori_KNL_16node
#SBATCH -o WOOD_DECAYERS_cori_KNL_16node.o%j

HIPMCL_EXE=../../bin/hipmcl
export OMP_NUM_THREADS=68
export OMP_PROC_BIND=true
export OMP_PLACES=cores
export MPICH_GNI_COLL_OPT_OFF=MPI_Alltoallv
N=16
n=16

# input in the matrix market format
IN_FILE=../../data/rwriley_WOOD_DECAYERS/MCL/output2
OUT_FILE=../../data/rwriley_WOOD_DECAYERS/MCL/output2.hipmcl

srun -N $N -n $n -c 68  --ntasks-per-node=1 --cpu_bind=cores $HIPMCL_EXE -M $IN_FILE -I 2 -per-process-mem 96 -o $OUT_FILE

